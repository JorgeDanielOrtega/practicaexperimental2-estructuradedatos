/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import ejercicio2.modelo.Color;
import ejercicio2.modelo.Raza;
import java.lang.reflect.Field;
import javax.swing.JComboBox;

/**
 *
 * @author daniel
 */
public class Utilidades {

    private static final Integer A_MINUSCULA = 97;
    private static final Integer Z_MINUSCULA = 122;
    private static final Integer A_MAYUSCULA = 65;
    private static final Integer Z_MAYUSCULA = 90;
    private static final Integer ESPACIO = 32;

    public static void rellenarCombo(JComboBox cmb, Integer seleccion) {
        cmb.removeAllItems();
        if (seleccion == 1) {
            cmb.addItem("ASCENDENTE");
            cmb.addItem("DESCENDENTE");
        } else if (seleccion == 2) {
            cmb.addItem("SHELL");
            cmb.addItem("QUICKSORT");
        } else if (seleccion == 3) {
            cmb.addItem("BINARIO");
            cmb.addItem("BINARIO-LINEAL");
        } else if (seleccion == 4) {
            for (Raza r : Raza.values()) {
                cmb.addItem(r);
            }
        } else if (seleccion == 5) {
            for (Color c : Color.values()) {
                cmb.addItem(c);
            }
        } else if(seleccion == 6){
            cmb.addItem("HEMBRA");
            cmb.addItem("MACHO");
        }else{
            cmb.addItem("id");
            cmb.addItem("nombre");
            cmb.addItem("raza");
            cmb.addItem("edadAnios");
            cmb.addItem("color");
            cmb.addItem("genero");
        }
    }

    public static Boolean isNumber(Class clase) {
        return clase.getSuperclass().getSimpleName().equalsIgnoreCase("Number");
    }

    public static Boolean isString(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("String");
    }

    public static Boolean isCharacter(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Character");
    }

    public static Boolean isBoolean(Class clase) {
        return clase.getSimpleName().equalsIgnoreCase("Boolean");
    }

    public static Boolean isPrimitive(Class clase) {
        return clase.isPrimitive();
    }

    public static Boolean isObject(Class clase) {
        return (!isBoolean(clase) && !isCharacter(clase)
                && !isNumber(clase) && !isString(clase) && !isPrimitive(clase)
                && !isEnum(clase));
    }

    public static Boolean isEnum(Class clase) {
        return clase.getSuperclass().getSimpleName().toLowerCase().contains("enum");
    }

    public static Field obtenerAtributo(Class clase, String nombre) {
        Field atributo = null;
        for (Field aux : clase.getDeclaredFields()) {
            if (aux.getName().equalsIgnoreCase(nombre)) {
                atributo = aux;
                break;
            }
        }
        return atributo;
    }

    public static Boolean hayCaracteresNoValidos(String cadena) {
        if (!cadena.trim().equals("")) {
            char[] cadenaChar = cadena.trim().toCharArray();
            for (int i = 0; i < cadenaChar.length; i++) {
                int posicionAscii = (int) cadenaChar[i];
                if (!((posicionAscii >= A_MAYUSCULA && posicionAscii <= Z_MAYUSCULA)
                        || (posicionAscii >= A_MINUSCULA && posicionAscii <= Z_MINUSCULA)
                        || posicionAscii == ESPACIO)) {
                    return true;
                }
            }
        }
        return false;
    }

}
