/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1.controlador.listas.Exceptions;

/**
 *
 * @author daniel
 */
public class ListOutLimitException extends Exception{
    public ListOutLimitException(String msg){
        super(msg);
    }

    public ListOutLimitException(Integer pos) {
        super("La posicion: " + pos + " sobrepasa los limites de la lista");
    }
    
    
}
