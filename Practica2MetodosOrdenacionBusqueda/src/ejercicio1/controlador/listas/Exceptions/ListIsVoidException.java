/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1.controlador.listas.Exceptions;

/**
 *
 * @author daniel
 */
public class ListIsVoidException extends Exception {
    public ListIsVoidException(String msg){
        super(msg);
    }
    
     public ListIsVoidException(){
        super("La lista esta vacia");
    }
}
