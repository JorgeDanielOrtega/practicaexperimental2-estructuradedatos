/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1.controlador;

import ejercicio1.controlador.listas.ListaEnlazada;
import ejercicio1.modelo.Lista;

/**
 *
 * @author daniel
 */
public class ControladorListaNumeros {

    Lista lista = new Lista();

    public ListaEnlazada<Float> generarNumeros() {
        try {
            ListaEnlazada<Float> listaFLoat = new ListaEnlazada<>();
            for (int i = 0; i < 20000; i++) {
                Double temp = Math.random() * 8000;
                Float tempAux =  Float.valueOf(String.valueOf(Math.round(temp*1000)));
                Float res = tempAux/1000;
                listaFLoat.insertar(res);
            }

//            listaFLoat.ordenacionShell(2, null);
//            listaFLoat ListaEnlazada<Float> listaFLoat = new ListaEnlazada<>();.print();
            return listaFLoat;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public ListaEnlazada<Float> ordenarLista(String algoritmo, String ordenacion) {
        try {
            Integer tipoOrdenacion = (ordenacion.equalsIgnoreCase("ascendente")) ? 1 : 2;
            if (algoritmo.equalsIgnoreCase("shell")) {
                return lista.getListaNumeros().ordenacionShell(tipoOrdenacion, null);
            } else {
                return lista.getListaNumeros().ordenarQuickSort(tipoOrdenacion, null);
            }
//            return lista.getListaNumeros();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

//    public static void main(String[] args) {
//        try {
//
//            Random rm = new Random();
//            ListaEnlazada<Float> listaFLoat = new ListaEnlazada<>();
//            for (int i = 0; i < 200; i++) {
//                listaFLoat.insertar((rm.nextFloat() * 5000));
//            }
//            listaFLoat.ordenacionShell(2, null);
//            listaFLoat.print();
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//            e.printStackTrace();
//        }
//    }
    public Lista getLista() {
        return lista;
    }

    public void setLista(Lista lista) {
        this.lista = lista;
    }

}
