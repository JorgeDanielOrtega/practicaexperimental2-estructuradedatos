/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1.vista.modeloTabla;

import ejercicio1.controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author daniel
 */
public class ModeloTablaNumeros extends AbstractTableModel {

    private ListaEnlazada<Float> lista = new ListaEnlazada<>();

    public ListaEnlazada<Float> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Float> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Numeros";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            switch (columnIndex) {
                case 0:
                    return lista.obtener(rowIndex);
                default:
                    return null;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
