/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1.modelo;

import ejercicio1.controlador.listas.ListaEnlazada;

/**
 *
 * @author daniel
 */
public class Lista {

    ListaEnlazada<Float> listaNumeros = new ListaEnlazada<>();

    public Lista() {
    }

    public ListaEnlazada<Float> getListaNumeros() {
        return listaNumeros;
    }

    public void setListaNumeros(ListaEnlazada<Float> listaNumeros) {
        this.listaNumeros = listaNumeros;
    }

}
