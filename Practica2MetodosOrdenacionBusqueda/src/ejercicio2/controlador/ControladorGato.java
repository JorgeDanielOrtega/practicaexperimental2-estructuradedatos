/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.controlador;

import ejercicio1.controlador.listas.ListaEnlazada;
import ejercicio2.modelo.Color;
import ejercicio2.modelo.Gato;
import ejercicio2.modelo.Raza;

/**
 *
 * @author daniel
 */
public class ControladorGato {

    public static final transient String NOMBRE_ARCHIVO = "gatos";
    private ListaEnlazada<Gato> gatoList = new ListaEnlazada<>();

    public ControladorGato() {
//        inicializarLista();
    }
//
//    private void inicializarLista() {
//        gatoList.vaciar();
//        gatoList.insertar(new Gato(2, "Alex", Raza.BENGALI, 1.2f, Color.BLANCO, 'M'));
//        gatoList.insertar(new Gato(4, "Felix", Raza.BALINES, 0.9f, Color.ROJIZO, 'H'));
//        gatoList.insertar(new Gato(11, "Tiger", Raza.ABISINIO, 1.5f, Color.NEGRO, 'M'));
//        gatoList.insertar(new Gato(5, "Zeus", Raza.ASIATICO, 0.11f, Color.ROJIZO, 'H'));
//        gatoList.insertar(new Gato(6, "Tom", Raza.BALINES, 1.5f, Color.NEGRO, 'M'));
//        gatoList.insertar(new Gato(1, "Figaro", Raza.ASIATICO, 1.2f, Color.BLANCO, 'M'));
//        gatoList.insertar(new Gato(3, "Felix", Raza.ASIATICO, 0.1f, Color.NEGRO, 'H'));
//        gatoList.insertar(new Gato(7, "Leo", Raza.BALINES, 1.7f, Color.AMARILLO, 'M'));
//        gatoList.insertar(new Gato(8, "Milo", Raza.BALINES, 1.2f, Color.NEGRO, 'M'));
//        gatoList.insertar(new Gato(12, "Queso", Raza.ASIATICO, 1.10f, Color.AMARILLO, 'M'));
//        gatoList.insertar(new Gato(10, "Gatsby", Raza.BALINES, 1.2f, Color.NEGRO, 'H'));
//        gatoList.insertar(new Gato(17, "Humo", Raza.BALINES, 0.2f, Color.BLANCO, 'M'));
//        gatoList.insertar(new Gato(13, "Luigi.", Raza.BENGALI, 0.1f, Color.NEGRO, 'H'));
//        gatoList.insertar(new Gato(15, "Choco", Raza.ABISINIO, 0.5f, Color.NEGRO, 'H'));
//        gatoList.insertar(new Gato(14, "Fuego", Raza.BALINES, 1.1f, Color.ROJIZO, 'M'));
//        gatoList.insertar(new Gato(9, "Rocky", Raza.ABISINIO, 1.2f, Color.BLANCO, 'H'));
//        gatoList.insertar(new Gato(16, "Blacky", Raza.BALINES, 1.8f, Color.NEGRO, 'M'));
//        gatoList.insertar(new Gato(20, "Romeo", Raza.ABISINIO, 1.0f, Color.AMARILLO, 'M'));
//        gatoList.insertar(new Gato(18, "Otoño", Raza.BALINES, 1.0f, Color.ROJIZO, 'H'));
//        gatoList.insertar(new Gato(19, "Romeo", Raza.BENGALI, 1.6f, Color.AMARILLO, 'M'));
//    }

    public void insertarNuevoGato(String nombre, Object raza, String genero, Object color, Float edad) {
        Gato aux = new Gato(gatoList.getSize() + 1, nombre, (Raza) raza, edad, (Color) color, genero.toCharArray()[0]);
        System.out.println(aux);
        getGatoList().insertar(aux);
    }

    public void buscar(Integer tipoBusqueda, String elemento, String atributo) {
        try {
            Integer id = null;
            String nombre = null;
            Raza raza = null;
            Color color = null;
            Float edad = null;
            Character genero = null;
            
            Object temp = null;
            
            if (atributo.equalsIgnoreCase("id")) {
                id = Integer.valueOf(elemento);
            }else if(atributo.equalsIgnoreCase("nombre")){
                nombre = elemento;
            }else if (atributo.equalsIgnoreCase("raza")){
                raza = Raza.valueOf(elemento.toUpperCase());
            }else if (atributo.equalsIgnoreCase("color")){
                color = Color.valueOf(elemento.toUpperCase());
            }else if(atributo.equalsIgnoreCase("edadAnios")){
                edad = Float.valueOf(elemento);
            }else if (atributo.equalsIgnoreCase("genero")){
                genero = elemento.toUpperCase().toCharArray()[0];
            }
            ListaEnlazada<Gato> aux = new ListaEnlazada<>();
            if (tipoBusqueda == 1) {
                aux.insertar((Gato) this.getGatoList().busquedaBinaria(new Gato(id, nombre, raza, edad, color, genero), false, atributo));
            } else {
                ListaEnlazada<Object> auxObject = getGatoList().busquedaBinariaLineal(new Gato(id, nombre, raza, edad, color, genero), false, atributo);
                for (int i = 0; i < auxObject.getSize(); i++) {
                    aux.insertar((Gato) auxObject.obtener(i));
                }
            }
            setGatoList(aux);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public ListaEnlazada<Gato> getGatoList() {
        return gatoList;
    }

    public void setGatoList(ListaEnlazada<Gato> gatoList) {
        this.gatoList = gatoList;
    }

}
