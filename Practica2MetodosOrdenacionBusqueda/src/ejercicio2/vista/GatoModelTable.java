/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.vista;

import ejercicio1.controlador.listas.ListaEnlazada;
import ejercicio2.modelo.Gato;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author daniel
 */
public class GatoModelTable extends AbstractTableModel{
      private ListaEnlazada<Gato> lista = new ListaEnlazada<>();

    public ListaEnlazada<Gato> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Gato> lista) {
        this.lista = lista;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public int getRowCount() {
        return lista.getSize();
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Id";
            case 1:
                return "Nombre";
            case 2:
                return "Raza";
            case 3:
                return "Color";
            case 4:
                return "Edad(Años)";
            case 5:
                return "Genero";
            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Gato gato = null;
        try {
            gato = lista.obtener(rowIndex);
        } catch (Exception e) {
        }
        switch (columnIndex) {
            case 0:
                return (gato != null) ? gato.getId().toString() : "NO DEFINIDO";
            case 1:
                return (gato != null) ? gato.getNombre() : "NO DEFINIDO";
            case 2:
                return (gato != null) ? gato.getRaza().toString(): "NO DEFINIDO";
            case 3:
                return (gato != null) ? gato.getColor().toString(): "NO DEFINIDO";
            case 4:
                return (gato != null) ? gato.getEdadAnios().toString(): "NO DEFINIDO";
            case 5:
                return (gato != null) ? gato.getGenero().toString(): "NO DEFINIDO";
            default:
                return null;
        }

    }

}
