/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.modelo;

/**
 *
 * @author daniel
 */
public class Gato {

    private Integer id;
    private String nombre;
    private Raza raza;
    private Float edadAnios;
    private Color color;
    private Character genero;

    public Gato() {
    }

    public Gato(Integer id, String nombre, Raza raza, Float edadAnios, Color color, Character genero) {
        this.id = id;
        this.nombre = nombre;
        this.raza = raza;
        this.edadAnios = edadAnios;
        this.color = color;
        this.genero = genero;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Raza getRaza() {
        return raza;
    }

    public void setRaza(Raza raza) {
        this.raza = raza;
    }

    public Float getEdadAnios() {
        return edadAnios;
    }

    public void setEdadAnios(Float edadAnios) {
        this.edadAnios = edadAnios;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Character getGenero() {
        return genero;
    }

    public void setGenero(Character genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "Gato{" + "id=" + id + ", nombre=" + nombre + ", raza=" + raza + ", edadAnios=" + edadAnios + ", color=" + color + ", genero=" + genero + '}';
    }

}
