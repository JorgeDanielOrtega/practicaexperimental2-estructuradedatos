/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2metodosordenacionbusqueda;

import ejercicio1.controlador.listas.ListaEnlazada;
import ejercicio2.controlador.ControladorGato;
import ejercicio2.modelo.Color;
import ejercicio2.modelo.Gato;
import ejercicio2.modelo.Raza;
import utilidades.FileJSON;

/**
 *
 * @author daniel
 */
public class Practica2MetodosOrdenacionBusqueda {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            ControladorGato controladorGato = new ControladorGato();
            utilidades.FileJSON f = new FileJSON(controladorGato.NOMBRE_ARCHIVO);
//        f.guardar(controladorGato);
            controladorGato = f.cargar(ControladorGato.class);
//        
//        controladorGato.getGatoList().busquedaLineal(new Gato(2, "Alex", Raza.BENGALI, 1.1f, Color.BLANCO, 'M'), false, "edadAnios").print();
//        controladorGato.getGatoList().busquedaBinariaLineal(new Gato(2, "Alex", Raza.BENGALI, 1.1f, Color.BLANCO, 'M'), false, "edadAnios").print();

//        controladorGato.getGatoList().ordenacionShell(1, "id");
            controladorGato.getGatoList().print();

            controladorGato.getGatoList().vaciar();
            controladorGato.getGatoList().insertar(new Gato(2, "Alex", Raza.BENGALI, 1.2f, Color.BLANCO, 'M'));
            controladorGato.getGatoList().insertar(new Gato(4, "Felix", Raza.BALINES, 0.9f, Color.ROJIZO, 'H'));
            controladorGato.getGatoList().insertar(new Gato(10, "Tiger", Raza.ABISINIO, 1.5f, Color.NEGRO, 'M'));
            controladorGato.getGatoList().insertar(new Gato(5, "Zeus", Raza.ASIATICO, 0.11f, Color.ROJIZO, 'H'));
            controladorGato.getGatoList().insertar(new Gato(6, "Tom", Raza.BALINES, 1.5f, Color.NEGRO, 'M'));
            controladorGato.getGatoList().insertar(new Gato(1, "Figaro", Raza.ASIATICO, 1.2f, Color.BLANCO, 'M'));
            controladorGato.getGatoList().insertar(new Gato(3, "Felix", Raza.ASIATICO, 0.1f, Color.NEGRO, 'H'));
            controladorGato.getGatoList().insertar(new Gato(7, "Leo", Raza.BALINES, 1.7f, Color.AMARILLO, 'M'));
            controladorGato.getGatoList().insertar(new Gato(8, "Milo", Raza.BALINES, 1.2f, Color.NEGRO, 'M'));
            controladorGato.getGatoList().insertar(new Gato(9, "Queso", Raza.ASIATICO, 1.10f, Color.AMARILLO, 'M'));

            controladorGato.getGatoList().print();

            f.guardar(controladorGato);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }

}
